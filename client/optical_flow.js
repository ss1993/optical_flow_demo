


function OpticalFlow(matrix, canvas, points_screen) {

    //Properties and initialization

    this.matrix = matrix;
    this.canvasWidth  = canvas.width;
    this.canvasHeight = canvas.height;
    this.ctx = canvas.getContext('2d');

    this.ctx.fillStyle = "rgb(0,255,0)";
    this.ctx.strokeStyle = "rgb(0,255,0)";

    this.curr_img_pyr = new jsfeat.pyramid_t(3);
    this.prev_img_pyr = new jsfeat.pyramid_t(3);
    this.curr_img_pyr.allocate(640, 480, jsfeat.U8_t|jsfeat.C1_t);
    this.prev_img_pyr.allocate(640, 480, jsfeat.U8_t|jsfeat.C1_t);

    this.point_count = points_screen.length;
    this.point_status = new Uint8Array(this.point_count);

    this.prev_xy = new Float32Array(this.point_count*2);
    //prev_xy = new Float32Array(100*2);
    this.curr_xy = new Float32Array(this.point_count*2);

    for(var i = 0; i<points_screen.length; i++) {
        this.curr_xy[i*2] = points_screen[i].x;
        this.curr_xy[(i*2)+1] = points_screen[i].y;
    };

};

OpticalFlow.prototype.drawCircle = function(ctx, x, y) {
    ctx.beginPath();
    ctx.arc(x, y, 4, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();
}

OpticalFlow.prototype.paintGoodPoints = function() {
    for(var i = 0; i < this.point_count; ++i) {
        if(this.point_status[i] == 1) {
            this.drawCircle(this.ctx, this.curr_xy[i<<1], this.curr_xy[(i<<1)+1]);
        }
    }
};

OpticalFlow.prototype.track = function () {
        //Mise a jour des coordonnes des points
        var imageData = this.ctx.getImageData(0, 0, 640, 480);
        var _pt_xy = this.prev_xy;
        this.prev_xy = this.curr_xy;
        this.curr_xy = _pt_xy;

        //Les deux pyramides ont que des 0
        var _pyr = this.prev_img_pyr;
        this.prev_img_pyr = this.curr_img_pyr;
        this.curr_img_pyr = _pyr;


        //Optical flow
        jsfeat.imgproc.grayscale(imageData.data, 640, 480, this.curr_img_pyr.data[0]);
        this.curr_img_pyr.build(this.curr_img_pyr.data[0], true);
        jsfeat.optical_flow_lk.track(this.prev_img_pyr, this.curr_img_pyr, this.prev_xy, this.curr_xy, this.point_count, 20, 30, this.point_status, 0.01, 0.001);
        this.paintGoodPoints();
};

OpticalFlow.prototype.applyTransform = function(M, pt) {
    var px = M[0]*pt.x + M[1]*pt.y + M[2];
    var py = M[3]*pt.x + M[4]*pt.y + M[5];
    var z = M[6]*pt.x + M[7]*pt.y + M[8];
    return {x:px/z,y:py/z};
}


