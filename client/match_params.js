match_params = function params(){
    var pri = {

    }
    var pub = {
        blur_size : 5, // +, - num_corners
        lap_thres : 30, // +, - num_corners
        eigen_thres : 25, // +, - num_corners
        match_threshold : 75, // +, + num_matches, + matching time
        nb_keypts : 250, //
        center_weight: 20,// 1 when no weighting
        num_train_levels : 3,
        max_pattern_size : 640,
        max_per_level : 250,
        sc_inc : Math.sqrt(3.0),
        nb_gmatch_thres: 8
    }
    return pub;
}