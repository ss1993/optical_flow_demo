function match(tParams){
	var pri = {

	}
	var pub = {
		match_t : (function () {
			function match_t(screen_idx, pattern_lev, pattern_idx, distance) {
				if (typeof screen_idx === "undefined") { screen_idx=0; }
				if (typeof pattern_lev === "undefined") { pattern_lev=0; }
				if (typeof pattern_idx === "undefined") { pattern_idx=0; }
				if (typeof distance === "undefined") { distance=0; }

				this.screen_idx = screen_idx;
				this.pattern_lev = pattern_lev;
				this.pattern_idx = pattern_idx;
				this.distance = distance;
			}
			return match_t;
		})(),
		ismatched : function(corners,descriptors,pattern_corners,pattern_descriptors,index, callback){
			var matches = [];
			var i = tParams.nb_keypts
			while(--i >= 0) {
				matches[i] = new this.match_t();
			}
			// startTime = new Date().getTime();
			var num_matches = this.match_pattern(descriptors,pattern_descriptors,tParams.num_train_levels,matches);
			var transform = this.find_transform(corners,pattern_corners,matches, num_matches);
			// console.log("Average matching time: "+(new Date().getTime()-startTime)+"ms");
			return ({matched: transform.cnt > tParams.nb_gmatch_thres, matrix: transform.transform_matrix, points_screen: transform.points_screen});
		},
		match_pattern: function (screen_descriptors,pattern_descriptors,num_train_levels,matches) {
			var q_cnt = screen_descriptors.rows;// = nb_keypts
			var query_du8 = screen_descriptors.data;
			var query_u32 = screen_descriptors.buffer.i32; // cast to integer buffer
			var qd_off = 0;
			var qidx=0,lev=0,pidx=0,k=0;
			var num_matches = 0;

			for(qidx = 0; qidx < q_cnt; ++qidx) {
				var best_dist = 256;
				var best_dist2 = 256;
				var best_idx = -1;
				var best_lev = -1;

				for(lev = 0; lev < num_train_levels; ++lev) {
					var lev_descr = pattern_descriptors[lev];
					var ld_cnt = lev_descr.rows;// = max_per_level
					var ld_i32 = lev_descr.buffer.i32; // cast to integer buffer
					var ld_off = 0;
					for(pidx = 0; pidx < ld_cnt; ++pidx) {
						var curr_d = 0;
						// our descriptor is 32 bytes so we have 8 Integers
						for(k=0; k < 8; ++k) {
							curr_d += this.popcnt32( query_u32[qd_off+k]^ld_i32[ld_off+k] );
						}
						if(curr_d < best_dist) {
							best_dist2 = best_dist;
							best_dist = curr_d;
							best_lev = lev;
							best_idx = pidx;
						} else if(curr_d < best_dist2) {
							best_dist2 = curr_d;
						}
						ld_off += 8; // next descriptor
					}
				}
				// filter out by some threshold
				if(best_dist < tParams.match_threshold) {
                    console.log(num_matches);
					matches[num_matches].screen_idx = qidx;
					matches[num_matches].pattern_lev = best_lev;
					matches[num_matches].pattern_idx = best_idx;
					num_matches++;
				}
				//

				/* filter using the ratio between 2 closest matches
				 if(best_dist < 0.8*best_dist2) {
				 matches[num_matches].screen_idx = qidx;
				 matches[num_matches].pattern_lev = best_lev;
				 matches[num_matches].pattern_idx = best_idx;
				 num_matches++;
				 }
				 */

				qd_off += 8; // next query descriptor
			}

			return num_matches;
		},
		find_transform: function (screen_corners,pattern_corners,matches, count) {// count = nb_matches
			// motion kernel
			var mm_kernel = new jsfeat.motion_model.homography2d();
			// ransac params
			var num_model_points = 4;
			var reproj_threshold = 3;
			var homo3x3 = new jsfeat.matrix_t(3,3,jsfeat.F32C1_t);
			var match_mask = new jsfeat.matrix_t(500,1,jsfeat.U8C1_t);
			var ransac_param = new jsfeat.ransac_params_t(num_model_points,reproj_threshold, 0.5, 0.99);
			var pattern_xy = [];
			var screen_xy = [];

			// construct correspondences
			for(var i = 0; i < count; ++i) {
				var m = matches[i];
				var s_kp = screen_corners[m.screen_idx];
				var p_kp = pattern_corners[m.pattern_lev][m.pattern_idx];
				pattern_xy[i] = {"x":p_kp.x, "y":p_kp.y};
				screen_xy[i] =  {"x":s_kp.x, "y":s_kp.y};
			}

			// estimate motion
			var ok = false;
			ok = jsfeat.motion_estimator.ransac(ransac_param, mm_kernel, pattern_xy, screen_xy, count, homo3x3, match_mask, 1000);
			// extract good matches and re-estimate
			var good_cnt = 0;
			if(ok) {
				for(var i=0; i < count; ++i) {
					if(match_mask.data[i]) {
						pattern_xy[good_cnt].x = pattern_xy[i].x;
						pattern_xy[good_cnt].y = pattern_xy[i].y;
						screen_xy[good_cnt].x = screen_xy[i].x;
						screen_xy[good_cnt].y = screen_xy[i].y;
						good_cnt++;
					}
				}
				// run kernel directly with inliers only
				mm_kernel.run(pattern_xy, screen_xy, homo3x3, good_cnt);
			} else {
				jsfeat.matmath.identity_3x3(homo3x3, 1.0);
			}

			return {cnt: good_cnt, transform_matrix: homo3x3, points_screen: screen_xy};
		},
		popcnt32: function (n) {
			n -= ((n >> 1) & 0x55555555);
			n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
			return (((n + (n >> 4))& 0xF0F0F0F)* 0x1010101) >> 24;
		}
	}
	return pub;
}

