/*
var Params = {
	blur_size : 5,
	lap_thres : 30,
	eigen_thres : 25,
	match_threshold : 48,
	nb_keypts : 200,
	num_train_levels : 4,
	max_pattern_size : 512,
	max_per_level : 300,
	sc_inc : Math.sqrt(2.0),
	nb_gmatch_thres: 8,
	yiboo:'http://localhost:6100/'
}
*/
var Params = {
	blur_size : 5,
	lap_thres : 30,
	eigen_thres : 25,
	match_threshold : 75,
	nb_keypts : 250,
	center_weight: 20,
	num_train_levels : 3,
	max_pattern_size : 640,
	max_per_level : 250,
	sc_inc : Math.sqrt(3.0),
	nb_gmatch_thres: 8,
	yiboo:'http://51.254.99.5:6100'
}

//yiboo:'http://51.254.99.5:6100/'