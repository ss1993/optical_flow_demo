var window = self;
importScripts("./params.js");
importScripts("./feature.js");
importScripts("./stonesJSON.js");
importScripts("./jsfeat-min.js");
importScripts("./match_params.js");
importScripts("./match.js");
var oFeature = new feature(Params);
self.onmessage = function (e) {
    var album_id = e.data.album_id;
    var imageData = e.data.imageData;
    var videoWidth = e.data.videoWidth;
    var videoHeight = e.data.videoHeight;
    var screen_descriptors = new jsfeat.matrix_t(32, Params.nb_keypts, jsfeat.U8_t | jsfeat.C1_t);
    var screen_corners = [];
    var oMatch = new match(new match_params());
    var i = Params.nb_keypts * 10;
    while (--i >= 0) {
        screen_corners[i] = new jsfeat.keypoint_t(0, 0, 0, 0, -1);
    }
    oFeature.describe(imageData, videoWidth, videoHeight, screen_corners, screen_descriptors);
    var data = {
        album_id: album_id,
        imageWidth: videoWidth,
        imageHeight: videoHeight,
        corners: screen_corners,
        descriptors: screen_descriptors
    };

    var pattern_corners = JSON.parse(s_pattern_corners);
    var pattern_descriptors = JSON.parse(s_pattern_descriptors);
    var matched = oMatch.ismatched(data.corners, data.descriptors, pattern_corners, pattern_descriptors);

    if (matched.matched) {
        console.log("Matched")
        self.postMessage({ismatched: 1, resultat: {
            matrix: matched.matrix,
            points_screen: matched.points_screen
        }})
    }
    else {
        self.postMessage({ismatched: 0});
    }


};
