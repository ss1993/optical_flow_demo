
function feature (tParams){
	var pri = {
		
	}
	var pub = {
		describe: function (imageData, imageWidth, imageHeight,corners,descriptors,index) {
			var img_u8 = new jsfeat.matrix_t(imageWidth, imageHeight, jsfeat.U8_t | jsfeat.C1_t);
			var img_u8_smooth = new jsfeat.matrix_t(imageWidth, imageHeight, jsfeat.U8_t | jsfeat.C1_t);
			jsfeat.imgproc.grayscale(imageData, imageWidth, imageHeight, img_u8);
			jsfeat.imgproc.gaussian_blur(img_u8, img_u8_smooth,tParams.blur_size|0);
			jsfeat.yape06.laplacian_threshold = tParams.lap_thres|0;
			jsfeat.yape06.min_eigen_value_threshold = tParams.eigen_thres|0;
			var num_corners = this.detect_keypoints_img(img_u8_smooth, corners, tParams.nb_keypts);
			// log[index].image_corners = num_corners;
			
			jsfeat.orb.describe(img_u8_smooth, corners, num_corners, descriptors);
		},
		detect_keypoints_img: function (img, corners, max_allowed) {
			// detect features
			var count = jsfeat.yape06.detect(img, corners, 0);
			// weighting to highlight central corners
			function weight(rx,ry,rw,hw){ // 0<r<1   1<weight<13
				if(rx < rw && ry < rw){
					return hw;
				}
				else if(rx >= rw && rx >= ry){
					return (rw + hw*(rx - 1) - rx)/(rw - 1);
				}
				else if(ry >= rw && rx<= ry){
					return (rw + hw*(ry - 1) - ry)/(rw - 1);
				}
				return 1;
			}
			for(var i = 0; i < count; ++i) {
				corners[i].score *= weight(Math.abs(corners[i].x - img.cols/2)/(img.cols/2) ,Math.abs(corners[i].y - img.rows/2)/(img.rows/2) ,0.5, tParams.center_weight);
				// if(!iscentered(corners[i],0.75,0.75)){corners[i].score = 0;}
			}
			// sort by score and reduce the count if needed
			jsfeat.math.qsort(corners, 0, count-1, function(a,b){return (b.score<a.score);});
			
			if(count > max_allowed) {
				count = max_allowed;
			}
			corners.splice(count,corners.length-count);//delete redundant corners
			// calculate dominant orientation for each keypoint
			for(var i = 0; i < count; ++i) {
				corners[i].angle = this.ic_angle(img, corners[i].x, corners[i].y);
			}
			// stream.write("Nombre de corners: "+count+"\r\n");
			return count;
		},
		detect_keypoints: function (img, corners, max_allowed) {
			// detect features
			var count = jsfeat.yape06.detect(img, corners, 0);
			
			// sort by score and reduce the count if needed
			if(count > max_allowed) {
				jsfeat.math.qsort(corners, 0, count-1, function(a,b){return (b.score<a.score);});
				count = max_allowed;
			}
			corners.splice(count,corners.length-count);//delete redundant corners
			
			// calculate dominant orientation for each keypoint
			for(var i = 0; i < count; ++i) {
				corners[i].angle = this.ic_angle(img, corners[i].x, corners[i].y);
			}
			// stream.write("Nombre de corners: "+count+"\r\n");
			return count;
		},
		ic_angle: function (img, px, py) {
			var u_max = new Int32Array([15,15,15,15,14,14,14,13,13,12,11,10,9,8,6,3,0]);
			var half_k = 15; // half patch size
			var m_01 = 0, m_10 = 0;
			var src=img.data, step=img.cols;
			var u=0, v=0, center_off=(py*step + px)|0;
			var v_sum=0,d=0,val_plus=0,val_minus=0;
			
			// Treat the center line differently, v=0
			for (u = -half_k; u <= half_k; ++u)
				m_10 += u * src[center_off+u];
			
			// Go line by line in the circular patch
			for (v = 1; v <= half_k; ++v) {
				// Proceed over the two lines
				v_sum = 0;
				d = u_max[v];
				for (u = -d; u <= d; ++u) {
					val_plus = src[center_off+u+v*step];
					val_minus = src[center_off+u-v*step];
					v_sum += (val_plus - val_minus);
					m_10 += u * (val_plus + val_minus);
				}
				m_01 += v * v_sum;
			}
			
			return Math.atan2(m_01, m_10);
		}
	}
	
	return pub;
}

