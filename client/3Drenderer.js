function ExtrapolatedHomographyCamera(pixelwidth,pixelheight){
    THREE.Camera.call( this );
    this.orthographicProjection = new THREE.Matrix4();
    this.orthographicProjection.makeOrthographic(0,pixelwidth,0,pixelheight,5,-5);
    this.intrinsic = CameraMatrixUtil.getDefaultIntrinsic(pixelwidth,pixelheight);
    console.log("ExtrapolatedHomographyCamera OK");
};
ExtrapolatedHomographyCamera.prototype = Object.create(THREE.Camera.prototype);
ExtrapolatedHomographyCamera.prototype.fromHomography = function fromMatrix(homography){
    var t = CameraMatrixUtil.extrapolate3DMatrix(homography,this.intrinsic,true,true);

    this.projectionMatrix.set(t[0][0], t[0][1], t[0][2], t[0][3],
        t[1][0], t[1][1], t[1][2], t[1][3],
        0   , 0   , 1   , 0   ,
        t[2][0], t[2][1], t[2][2], t[2][3]);
    this.projectionMatrix.multiplyMatrices(this.orthographicProjection,this.projectionMatrix);
};

var globalObjectCamera;

function threeDRenderer(){
    var backgroundSourceCanvas,
        backgroundScene,
        backgroundCamera,
        backgroundTexture,
        objectScene,
        objectCamera,
        renderer,
        pixelwidth,
        pixelheight;

    var cubeRenderer = {
        setup: function setup(renderCanvas,pwidth,pheight){
            pixelwidth = pwidth;
            pixelheight = pheight;

            //Sets up the renderer
            backgroundScene = new THREE.Scene();
            // backgroundCamera = new THREE.PerspectiveCamera(75,640.0/360.0,0.1,1000);

            // backgroundScene.add(backgroundCamera);

            renderer = new THREE.WebGLRenderer({canvas:renderCanvas, antialias: true, alpha: true });
            renderer.setSize( screen.width, screen.height );
            //   renderer.setClearColor( 0xffffff, 0);

            var geometry = new THREE.BoxGeometry( 200, 200, 200 );
            var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
            var cube = new THREE.Mesh( geometry, material );
            backgroundScene.add( cube );

            //backgroundCamera.position.z = 5;
            objectCamera=new ExtrapolatedHomographyCamera(pixelwidth,pixelheight);

            backgroundScene.add( objectCamera );
            console.log("threeDRenderer setup done");

        },
        draw: function(matrix){
            //renderer.setClearColor( 0xffffff, 0);
            renderer.autoClear = false;
            renderer.clear();
            renderer.render(backgroundScene, objectCamera);
            if(matrix){
                objectCamera.fromHomography(matrix.data);
                //    renderer.setClearColor( 0xffffff, 0);
                renderer.render(backgroundScene, objectCamera);
            }
        }
    };

    return cubeRenderer;
}