'use strict';



// navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

var videoElement = document.querySelector('video');
var videoSwitch = document.querySelector('input#cameraswitch');
var videoSource = [];

var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0,
	isFirefox = typeof InstallTrigger !== 'undefined',
	isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0,
	isChrome = !!window.chrome && !isOpera,
	isIE = /*@cc_on!@*/false || !!document.documentMode;
if(isOpera){
	
}
else if(isFirefox){
	compatibility.getUserMedia({
			video: true}, function(stream) {
		try {
			window.stream = stream;
			videoElement.src = compatibility.URL.createObjectURL(stream);
		} catch (error) {
			videoElement.src = stream;
		}
		setTimeout(function() {
				videoElement.play();
			}, 500);
	}, function (error) {
		console.log(error+"asd");
		document.querySelector('canvas#canvas').style.display = 'none';
	});
}
else if(isSafari){
	document.querySelector('input#cameraswitch').style.display = 'none';
}
else if(isChrome){
	if (typeof MediaStreamTrack === 'undefined'||typeof MediaStreamTrack.getSources === 'undefined'){
	alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
	} else {
		MediaStreamTrack.getSources(function(sourceInfos){
			for (var i = 0; i !== sourceInfos.length; ++i) {
				if (sourceInfos[i].kind === 'video') {
					videoSource.push(sourceInfos[i].id);
				} 
			}
			start();
		});
		videoSwitch.onclick = switchcamera;
	}
}
else if(isIE){
	compatibility.getUserMedia({
			video: true}, function(stream) {
		try {
			window.stream = stream;
			videoElement.src = compatibility.URL.createObjectURL(stream);
		} catch (error) {
			videoElement.src = stream;
		}
		setTimeout(function() {
				videoElement.play();
			}, 500);
	}, function (error) {
		console.log(error+"asd");
		document.querySelector('canvas#canvas').style.display = 'none';
	});
}

function start(){
	if (!!window.stream) {
		videoElement.src = null;
		window.stream.stop();
	}
	if(videoSource.length===1){
		document.querySelector('div#cameraswitch-div').style.display = 'none';
		var constraints = {
			video: {
				mandatory: {
					minWidth: 480,
					minHeight: 320
				},
				optional: [{sourceId: videoSource[0]}]
			}
		};
	}
	else if(videoSource.length > 1){
		document.querySelector('div#cameraswitch-div').style.display = 'block';
		
		var constraints = {
			video: {
				mandatory: {
					minWidth: 640,
					minHeight: 480
				},
				optional: [{sourceId: videoSource[videoSource.length-1]}]
			}
		};
		
	}
	compatibility.getUserMedia(constraints, function (stream) {
		window.stream = stream; // make stream available to console
		videoElement.src = compatibility.URL.createObjectURL(stream);
		videoElement.play();
	}, function(){
		console.log('navigator.getUserMedia error: ', error);
	});
};
function switchcamera(){
	if (!!window.stream) {
		videoElement.src = null;
		window.stream.stop();
	}
	var videoid = videoSwitch.checked ? videoSource[videoSource.length-1]:videoSource[0];
	var mandatory = videoSwitch.checked ? {minWidth: 1280,minHeight: 720}:{maxWidth: 1280,maxHeight: 720};
	var constraints = {
		video: {
			mandatory: mandatory,
			optional: [{sourceId: videoid}]
		}
	};
	compatibility.getUserMedia(constraints, function (stream) {
		window.stream = stream; // make stream available to console
		videoElement.src = window.URL.createObjectURL(stream);
		videoElement.play();
	}, function(){
		console.log('navigator.getUserMedia error: ', error);
		document.querySelector('canvas#canvas').style.display = 'none';
	});
}


